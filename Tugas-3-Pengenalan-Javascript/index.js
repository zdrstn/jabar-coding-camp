
// soal 1

var pertama = 'saya sangat senang hari ini';
var kedua = 'belajar javascript itu keren';

// jawaban

var a = pertama.substring ( 0, 4 )
var b = pertama.substr ( 12, 6 )
var c = kedua.substring ( 0, 7 )
var d = kedua.substr ( 8, 10).toUpperCase ()
var kalimat = a + ' ' + b + ' ' + c +' ' + d

console.log(kalimat)


// soal 2
var katapertama = '10';
var katakedua = '2' ;
var kataketiga= '4';
var katakeempat= '6';

var jawabankesatu = parseInt(katapertama)
var jawabankedua = parseInt(katakedua)
var jawabanketiga= parseInt(kataketiga)
var jawabankeempat = parseInt(katakeempat)

var jawaban = jawabankesatu + jawabankedua * jawabanketiga + jawabankeempat

console.log(jawaban)




// soal 3

var kalimat = 'wah javascript itu keren sekali'

var katapertama = kalimat.substring(0, 3);
var katakedua = kalimat.substr(4, 10);
var kataketiga = kalimat.substring(15, 18);
var katakeempat = kalimat.substring(19, 24);
var katakelima = kalimat.substring(25, 31);

console.log('kata pertama: '+ katapertama);
console.log('kata kedua: '+ katakedua);
console.log('kata ketiga: '+ kataketiga);
console.log('kata keempat: '+ katakeempat);
console.log('kata kelima: '+ katakelima);
